#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from ackermann_msgs.msg import AckermannDriveStamped
import DriveControl


def callback(data):
    a_d_s = driveControl.update_driving(steering=data.axes[0], accelerator=data.axes[4], brake=data.axes[5])
    pub.publish(a_d_s)


def start_mappper_node():
    # publishing to "vesc/ackermann_cmd_mux/navigation" to control robot
    global driveControl, pub

    driveControl = DriveControl.DriveControl(5.0)

    pub = rospy.Publisher('vesc/ackermann_cmd_mux/input/navigation', AckermannDriveStamped, queue_size=10)
    # subscribed to joystick inputs on topic "joy"
    rospy.Subscriber("joy", Joy, callback)
    # starts the node
    rospy.init_node('joy2robot')
    # rate = rospy.Rate(10)
    rospy.spin()


if __name__ == '__main__':
    try:
        start_mappper_node()
    except rospy.ROSInterruptException:
        pass
