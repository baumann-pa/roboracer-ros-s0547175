#!/usr/bin/env python
from ackermann_msgs.msg import AckermannDriveStamped

STEERING_ANGLE_MAX = 1.0
AXES_NOT_TRIGGERED = 1.0  # is 1 since Button has value 1 for not being triggered
ANGLE_DEAD = 0.05


class DriveControl:
    def __init__(self, max_speed=0.0):
        self.max_speed = max_speed  # speed in m/s
        self.steering_angle = 0.0
        self.speed = 0.0
        self.accelerator = AXES_NOT_TRIGGERED
        self.brake = AXES_NOT_TRIGGERED

    def update_driving(self, steering=0.0, accelerator=AXES_NOT_TRIGGERED, brake=AXES_NOT_TRIGGERED):
        self._update_steering(steering)
        self._update_speed(accelerator, brake)
        # self.steering_angle = steering

        a_d_s = AckermannDriveStamped()
        a_d_s.drive.steering_angle = self.steering_angle
        a_d_s.drive.steering_angle_velocity = 0.0
        a_d_s.drive.acceleration = 0.0
        a_d_s.drive.speed = self.speed
        a_d_s.drive.jerk = 0.0

        return a_d_s

    def _update_speed(self, accelerator=AXES_NOT_TRIGGERED, brake=AXES_NOT_TRIGGERED):
        if brake == -AXES_NOT_TRIGGERED:
            self._reverse(accelerator)
            return

        # TODO use velocity together with speed and implement more realistic acceleration and useful brake
        # elif brake != AXES_NOT_TRIGGERED:
        #     self._brake(brake)
        #     return
        else:
            self._accelerate(accelerator)
            return

    def _update_steering(self, steering=0.0):
        if steering > STEERING_ANGLE_MAX:
            self.steering_angle = STEERING_ANGLE_MAX
        elif steering < -STEERING_ANGLE_MAX:
            self.steering_angle = -STEERING_ANGLE_MAX
        elif -ANGLE_DEAD < steering and steering < ANGLE_DEAD:
            self.steering_angle = 0.0
        else:
            self.steering_angle = steering

    def _reverse(self, accelerator=1.0):
        if accelerator > 0:
            self.speed = -1 * ((self.max_speed / 2) - ((self.max_speed / 2) * accelerator))
        else:
            self.speed = -1 * ((self.max_speed / 2) + ((self.max_speed / 2) * abs(accelerator)))

    def _brake(self, brake=1.0):
        if brake < 0:
            self.speed = (self.max_speed / 2) - ((self.max_speed / 2) * abs(brake))
        else:
            self.speed = (self.max_speed / 2) + ((self.max_speed / 2) * brake)

    def _accelerate(self, accelerator=-1.0):
        if accelerator > 0:
            self.speed = (self.max_speed / 2) - ((self.max_speed / 2) * accelerator)
        else:
            self.speed = (self.max_speed / 2) + ((self.max_speed / 2) * abs(accelerator))
